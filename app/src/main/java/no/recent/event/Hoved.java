package no.recent.event;

import android.app.ActionBar;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Locale;

public class Hoved extends AppCompatActivity {
    private Button visEventerKnapp, nyEventKnapp, taBildeKnapp, visKartKnapp;
    Locale minLocale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hoved);

        // deklarerer hvor knappene ligger på xml
        visEventerKnapp = (Button) findViewById(R.id.visEventerKnapp);
        nyEventKnapp = (Button) findViewById(R.id.leggTilEventKnapp);
        taBildeKnapp = (Button) findViewById(R.id.taBildeKnapp);
        visKartKnapp = (Button) findViewById(R.id.kartKnapp);

        // Går til Kart klassen når knappen blir trykket
        visKartKnapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent minIntent = new Intent(Hoved.this, Kart.class);
                startActivity(minIntent);
            }
        });

        taBildeKnapp.setOnClickListener(new View.OnClickListener() {
            @Override
        public void onClick(View v) {
                Intent kameraIntent = getPackageManager().getLaunchIntentForPackage("com.android.camera");
                startActivity(kameraIntent);
            }
        });

        visEventerKnapp.setOnClickListener(new View.OnClickListener() {
            @Override
        public void onClick(View v) {
                Intent visEventIntent = new Intent(Hoved.this, Eventer.class);
                startActivity(visEventIntent);
            }
        });

        nyEventKnapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nyEventIntent = new Intent(Hoved.this, NyEvent.class);
                startActivity(nyEventIntent);
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_innlogging, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.norskFlagg:
                Toast.makeText(getApplicationContext(),
                        "Du har forandret språket til Norsk!", Toast.LENGTH_SHORT)
                        .show();
                setLocale("nb");
                return true;
            case R.id.engelskFlagg:
                Toast.makeText(getApplicationContext(),
                        "You have changed the language to English!", Toast.LENGTH_SHORT)
                        .show();
                setLocale("en");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void setLocale(String lang) {
        minLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = minLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, Hoved.class);
        startActivity(refresh);
    }
}
