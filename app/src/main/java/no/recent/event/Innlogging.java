package no.recent.event;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Locale;

public class Innlogging extends AppCompatActivity {
    private TextView textView;
    private EditText brukernavn;
    private EditText passord;
    private Button logginnKnapp;
    private Button registrerKnapp;
    private String baadeBrukerOgPassordInntastet = "{\"brukernavn\":\"";
    private String heleBrukerDatabasen;
    TextView compare;
    Locale minLocale;

    private boolean erLoggetinn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_innlogging);

        brukernavn = (EditText) findViewById(R.id.brukernavnInn);
        passord = (EditText) findViewById(R.id.passordInn);

        textView = (TextView) findViewById(R.id.jasontekst);
        getJSON task = new getJSON();
        task.execute(new String[] {"http://www.tnsoft.no/android/jsonout.php"});
        textView.setVisibility(View.INVISIBLE);


        // tester JSON
        /* Button testKnapp = (Button) findViewById(R.id.testKnapp);
        testKnapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent minIntent = new Intent(Innlogging.this, WebApi.class);
                startActivity(minIntent);
            }
        });*/

        logginnKnapp = (Button) findViewById(R.id.logginnKnapp);
        registrerKnapp = (Button) findViewById(R.id.registrerKnapp);

        // går til Registrer Activity
        registrerKnapp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent minIntent = new Intent(Innlogging.this, Registrer.class);
                startActivity(minIntent);
            }
        });

        // Når logginnKnappen trykkes
        logginnKnapp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                brukernavn = (EditText) findViewById(R.id.brukernavnInn);
                passord = (EditText) findViewById(R.id.passordInn);

                String brukerStr = brukernavn.getText().toString();
                String passordStr = passord.getText().toString();

                // Man kan bruke StringBuilder eller StringJoiner her også
                baadeBrukerOgPassordInntastet+=brukerStr;
                baadeBrukerOgPassordInntastet+="\",\"passord\":\"";
                baadeBrukerOgPassordInntastet+=passordStr;
                baadeBrukerOgPassordInntastet+="\"}";
                heleBrukerDatabasen = textView.getText().toString();



                // riktig brukernavn og passord
                //if(brukernavn.getText().toString().equals("halla")) {
                if (heleBrukerDatabasen.contains(baadeBrukerOgPassordInntastet)) {
                    Toast.makeText(getApplicationContext(), R.string.duerloggetinn, Toast.LENGTH_SHORT).show();
                    Intent minIntent = new Intent(Innlogging.this, Hoved.class);
                     startActivity(minIntent);
                } else if (!heleBrukerDatabasen.contains(baadeBrukerOgPassordInntastet)){
                    baadeBrukerOgPassordInntastet = "{\"brukernavn\":\"";
                    Toast.makeText(getApplicationContext(), R.string.duerikkeloggetinn, Toast.LENGTH_SHORT).show();

                    // compare.setText(baadeBrukerOgPassordInntastet);
                    // feil brukernavn og passord
                }
            }
        });

    }

    // kode funnet fra pdf til faglærer - uke 45
    public class getJSON extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            String string = "";
            String output = "";
            for (String url : urls) {
                try {
                    URL urlen = new URL(urls[0]);
                    HttpURLConnection conn = (HttpURLConnection) urlen.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setRequestProperty("Accept", "application/json");

                    if (conn.getResponseCode() != 200) {
                        throw new RuntimeException("Feilet: HTTP feilkode:"
                                +conn.getResponseCode());
                    }

                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            (conn.getInputStream())));

                    System.out.println("Output fra Server ... \n");
                    while ((string = br.readLine()) != null) {
                        output = output + string;
                    }

                    conn.disconnect();
                    return output;
                } catch (Exception e) {
                    return "Noe gikk feil";
                }
            }
            return output;
        }

        @Override
        protected void onPostExecute(String str) {

            textView.setText(str);
        }
    }

    private void logginn() {

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_innlogging, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.norskFlagg:
                Toast.makeText(getApplicationContext(),
                        "Du har forandret språket til Norsk!", Toast.LENGTH_SHORT)
                        .show();
                setLocale("nb");
                return true;
            case R.id.engelskFlagg:
                Toast.makeText(getApplicationContext(),
                        "You have changed the language to English!", Toast.LENGTH_SHORT)
                        .show();
                setLocale("en");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void setLocale(String lang) {
        minLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = minLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, Innlogging.class);
        startActivity(refresh);
    }
}
