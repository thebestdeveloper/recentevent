package no.recent.event;

import android.app.ActionBar;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Locale;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import android.content.res.Resources;
import android.content.res.Configuration;

public class Registrer extends AppCompatActivity {
    private EditText brukernavn, passord;
    private Button registrerKnapp, norskKnapp, engelskKnapp;
    private TextView textView;
    Locale minLocale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrer);

        brukernavn = (EditText) findViewById(R.id.brukernavnInn);
        passord = (EditText) findViewById(R.id.passordInn);
        registrerKnapp = (Button) findViewById(R.id.registrerKnapp);

        registrerKnapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Ønsker å ha et bruker navn med 3 eller flere tegn
                if (brukernavn.length() <= 2) {
                    Toast.makeText(Registrer.this, "Du må ha med brukernavn for å registrere", Toast.LENGTH_SHORT).show();
                } else if (passord.length() <= 3) { // vil ha et passord med over 3 tegn
                    Toast.makeText(Registrer.this, "Du må ha med passord for å registrere", Toast.LENGTH_SHORT).show();
                } else {

                    Toast.makeText(Registrer.this, "Du er registrert! Logg inn nå!", Toast.LENGTH_SHORT).show();
                    getJSON task = new getJSON();
                    task.execute(new String[]{"http://tnsoft.no/android/jsonin.php/?brukernavn=" + brukernavn.getText().toString() + "&&passord=" + passord.getText().toString()});

                    Intent minIntent = new Intent(Registrer.this, Innlogging.class);
                    startActivity(minIntent);
                }

            }
        });


    }

    // kode funnet fra pdf til faglærer - uke 45
    public class getJSON extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            String string = "";
            String output = "";
            for (String url : urls) {
                try {
                    URL urlen = new URL(urls[0]);
                    HttpURLConnection conn = (HttpURLConnection) urlen.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setRequestProperty("Accept", "application/json");

                    if (conn.getResponseCode() != 200) {
                        throw new RuntimeException("Feilet: HTTP feilkode:"
                                +conn.getResponseCode());
                    }

                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            (conn.getInputStream())));

                    System.out.println("Output fra Server ... \n");
                    while ((string = br.readLine()) != null) {
                        output = output + string;
                    }

                    conn.disconnect();
                    return output;
                } catch (Exception e) {
                    return "Noe gikk feil";
                }
            }
            return output;
        }

        @Override
        protected void onPostExecute(String str) {

            // textView.setText(str);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_innlogging, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.norskFlagg:
                Toast.makeText(getApplicationContext(),
                        "Du har forandret språket til Norsk!", Toast.LENGTH_SHORT)
                        .show();
                setLocale("nb");
                return true;
            case R.id.engelskFlagg:
                Toast.makeText(getApplicationContext(),
                        "You have changed the language to English!", Toast.LENGTH_SHORT)
                        .show();
                setLocale("en");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void setLocale(String lang) {
        minLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = minLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, Registrer.class);
        startActivity(refresh);
    }
}